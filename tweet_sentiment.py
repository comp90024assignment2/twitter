import string
import csv
import re
import preprocessor as p
import pickle
from nltk.tokenize import TweetTokenizer
from nltk.classify import NaiveBayesClassifier

# Read the Emoji Sentiment Data csv file, build the dictionary and store
# the object in pickle files, this allows us to load the emoji sentiments
# dictionary instal of building it every time the program is run
def build_emoji_sentiments():
    read_emoji_sentiment_data('Emoji_Sentiment_Data_v1.0.csv')
    with open('emoji_sentiments.pkl', 'wb') as output:
        pickle.dump(emoji_sentiments, output, pickle.HIGHEST_PROTOCOL)

# Read the Twitter Training Data csv file, build the Naive Bayes Classifier and
# store the object in pickle file, this allows us to load the classifier
# instead of building it every time the program is run
def build_classifier():
    training_set = read_training_data('Sentiment Analysis Dataset.csv')
    with open('classifier.pkl', 'wb') as output:
        classifier = NaiveBayesClassifier.train(training_set)
        pickle.dump(classifier, output, pickle.HIGHEST_PROTOCOL)

# Returns the Naive Bayes classifier object stored in the pickle file
def load_classifier():
    with open('classifier.pkl', 'rb') as input:
        return pickle.load(input)

# Returns the Emoji Sentiments dictionary object stored in the pickle file
def load_emoji_sentiments():
    with open('emoji_sentiments.pkl', 'rb') as input:
        return pickle.load(input)

# Returns either 'pos' or 'neg' based on the Naive Bayes classification on the tweet
def text_sentiment(classifier, tweet):
    return classifier.classify(tweet_preprocessing(tweet))

# Returns a score between -1 and 1 for all the emojis that appear in the tweet
# Below 0 is considered negative and above 0 is considered positiveself.
# Returns 0 if there are no emojis in the tweet or no sentiments are recorded for
# the emojis
def emojis_sentiment(tweet, emoji_sentiments):
    parsed_tweet = p.parse(tweet)
    sentiment = 0
    if parsed_tweet.emojis:
        for emoji in parsed_tweet.emojis:
            try:
                sentiment += emoji_sentiments[f'0x{ord(emoji.match):X}'.lower()]
            except:
                continue
        average_emoji_sentiment = sentiment / len(parsed_tweet.emojis) # average the emoji scores
        return round(average_emoji_sentiment, 3)
    else:
        return 0

# Returns a tokenized dictionary of the words in the tweet after removing unwanted
# strings including mentions, hashtags, urls, reserved words (RT, FAV), emojis
# smileys and punctuation.
def tweet_preprocessing(dirty_tweet):
    tokenizer = TweetTokenizer()
    clean_tweet = p.clean(dirty_tweet).translate(str.maketrans('','',string.punctuation))
    clean_tweet = re.sub(r"[^\w\d'\s]+",'',clean_tweet)
    clean_tweet = clean_tweet.lower()
    tweet_tokens = {word: True for word in tokenizer.tokenize(clean_tweet)}
    return tweet_tokens

# Reads the file and returns a list of lists containing the tokens for each
# tweet in the training data and the classification
def read_training_data(file):
    pos = []
    neg = []
    with open(file, 'r') as f:
        reader = csv.reader(f)
        next(reader)
        for row in reader:
            tokens = tweet_preprocessing(row[3])
            if row[1] == '0':
                neg.append([tokens, 'neg'])
            elif row[1] == '1':
                pos.append([tokens, 'pos'])

    f.close()
    return pos + neg

# Reads the file, calculates the sentiment score and returns a dictionary
# with key, value {'emoji_unicode': sentiment_score}
def read_emoji_sentiment_data(file):
    emoji_sentiments = {}
    with open(file, 'r') as f:
      reader = csv.reader(f)
      next(reader)
      for row in reader:
          unicode = row[1]
          negative = int(row[4])
          neutral = int(row[5])
          positive = int(row[6])

          total = negative + neutral + positive
          pos_sent = positive/total
          neg_sent = negative/total
          sentiment = pos_sent - neg_sent
          emoji_sentiments[unicode] = round(sentiment, 3)
    f.close()
    return emoji_sentiments
