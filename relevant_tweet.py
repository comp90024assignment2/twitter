import datetime
from pytz import timezone

TEN_HOURS = datetime.timedelta(hours=10)
AEST = timezone('Australia/Melbourne')


def get_match_id_winner(db, created_at, team_name):
    """
    Takes a created_at datetime from a tweet (as parsed by tweepy) and the team's name, and returns the corresponding
        match_id and result (or None otherwise)
    """

    all_matches = list(db.get_view_result(
        '_design/matches',
        view_name='allWinnersLosers',
        startkey=[team_name, (created_at - TEN_HOURS).timestamp()],
        endkey=[team_name, created_at.timestamp()],
        descending=False
    ))

    if len(all_matches) == 0:
        return None, None

    match_id = all_matches[0]['id']
    result = all_matches[0]['value']

    # Ensure that both time and team are relevant
    if team_name in match_id:
        return match_id, result
    else:
        return None, None


# Test case
if __name__ == '__main__':
    example_time = "Sat Sep 05 13:05:00 +1000 2015"
    example_datetime = datetime.datetime.strptime(example_time, '%a %b %d %H:%M:%S +1000 %Y')
    print(get_match_id_winner(example_datetime, 'Adelaide'))
    print(get_match_id_winner(example_datetime, 'Collingwood'))  # returns None
