import tweepy
from cloudant import couchdb
import datetime
import sys
import tweet_sentiment
import relevant_tweet
import time
import argparse
import requests

classifier = tweet_sentiment.load_classifier()
emoji_sentiments = tweet_sentiment.load_emoji_sentiments()

# Choose between two different API keys
if sys.argv[1] == '--alt-auth':
    auth = tweepy.OAuthHandler(
        "NVrEF85xVg1GllpUrwD7WpMaM",
        "u6pqzNCu6gcYXyJeTglraKZU9USHbsPxmdfoAizNj4zuvWEbbA"
    )
    auth.set_access_token(
        "2412483924-g7FA7KEnWfwx1R6E22FKPjpf5SlxRocap5O4wwz",
        "JG9IjciIoatNrzFNkjLjssHpeyrVOZHFJ8hblFME3AvKs"
    )
else:
    auth = tweepy.OAuthHandler(
        "GgQmdUPtFunsSTE03qimi3ekF",
        "r0lzLstRorac5DkHEussMtQHvJ51pM495YPIqAkdR6M1VCykDE"
    )
    auth.set_access_token(
        "988794657827045376-mD9p3UEylFlma8E9cb4LHAk35cVteIs",
        "F8JFZrniWEnv9Cj82YjucXySnyfDYaqrS7QPAYTeEQ8sk"
    )
api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

if not api:
    print("Can't Authenticate")
    sys.exit(-1)


def get_args():
    parser = argparse.ArgumentParser('Runs the AFL Scraper frontend')
    parser.add_argument(
        '--db-host',
        help='The URL at which to connect to the database',
        required=True
    )
    return parser.parse_args()


# break the while loop when either get the rate_limit or the user doesn't have tweets
def get_user_tweet(user, db):
    user_id = user['user_id']
    print(f'Scraping user {user_id}')

    # If we've already scraped this user, just get new tweets
    if user['last_scraped'] != 0 and 'newest_tweet' in user:
        print(f'We have scraped this user before, starting from {user["newest_tweet"]}')
        kwargs = dict(
            since_id=user['newest_tweet']
        )
    else:
        print(f'We have never scraped this user before, starting from the start')
        kwargs = {}

    # Add the new tweets
    tweets = []

    try:
        for tweet in tweepy.Cursor(
                api.user_timeline,
                user_id=user_id,
                include_rts=False,
                tweet_mode='extended',
                **kwargs
        ).items():

            # Store a copy of the tweet for each team the owner is in
            for team in user['teams']:
                print(f"User is in {team}. Adding tweet for this team")

                # If not timezone is specified, assume it's UTC
                if tweet.created_at.tzinfo is None:
                    created_at = tweet.created_at.replace(tzinfo=datetime.timezone.utc)
                else:
                    created_at = tweet.created_at

                # Get the relevant match and result for this tweet from the DB
                match, result = relevant_tweet.get_match_id_winner(db, created_at, team)

                tweets.append({
                    'id': tweet.id,
                    'type': 'tweet',
                    'user': user_id,
                    'text': tweet.full_text,
                    'sentiment': tweet_sentiment.text_sentiment(classifier, tweet.full_text),
                    'emoji_sentiment': tweet_sentiment.emojis_sentiment(tweet.full_text, emoji_sentiments),
                    'match': match,
                    'created_at': created_at.timestamp(),
                    'result': result,
                    'team': team
                })
        db.bulk_docs(tweets)

        # Update the user with the last scraped date (now), and the newest tweet
        user['last_scraped'] = datetime.datetime.utcnow().timestamp()
        if tweets:
            user['newest_tweet'] = tweets[-1]['id']
        db.create_document(user)

    except tweepy.TweepError as e:
        # If that user wasn't found or was private, pretend we scraped it, and move on
        print(e)
        user['last_scraped'] = datetime.datetime.utcnow().timestamp()
        db.create_document(user)
        return


if __name__ == '__main__':
    args = get_args()
    with couchdb('admin', 'Vai9vah6', url=args.db_host) as couch:
        print('connected...')
        db = couch['comp90024']

        # Loop forever
        while True:

            # If getting the view result times out, sleep to give CouchDB time to recalculate the view
            try:
                # Get all users, in order of last scraped, in ascending order
                for current_user in db.get_view_result(
                        '_design/users',
                        view_name='byLastScraped',
                        descending=False,
                        include_docs=True
                ):
                    get_user_tweet(current_user['doc'], db)
            except requests.exceptions.HTTPError as ex:
                print(ex)
                print("Sleeping for 20 seconds...")
                time.sleep(20)
                continue
